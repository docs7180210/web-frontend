### JSON

`JSON`是一种遵守特定格式的字符串，通常用于在不同环境之间交换数据

```JSON
{
  "key1":"string",
  "key2":100,
  "key3":[1,2,3],
  "key4":{"a":1000}
}
```

### 静态方法

| 方法               | 作用                           |
| ------------------ | ------------------------------ |
| `JSON.stringify()` | 将 `js`对象转换成 `JSON`字符串 |
| `JSON.parse()`     | 将`JSON`字符串转换成`js`对象   |

```javascript
var obj = {
  key1: 'value',
  key2: 100,
}

var strObj = JSON.stringify(obj)
console.log(strObj) // 输出 {"key1":"value","key2":100}
```

### 算术

`Math`拥有一些数学属性和数学计算函数

```javascript
// 圆周率
console.log(Math.PI) // 输出圆周率

// 计算 2 的 3 次方
var value = Math.pow(2, 3)
console.log(value) // 输出 8

// 四舍五入
var value = Math.round(1.56)
console.log(value) // 输出 2

// 取整
var value = Math.trunc(1.99)
console.log(value) // 输出 1
```

### 静态属性

| 属性         | 说明          |
| ------------ | ------------- |
| `Math.E`     | 欧拉常数      |
| `Math.LN2`   | 2 的自然对数  |
| `Math.LN10`  | 10 的自然对数 |
| `Math.SQRT2` | 2 的平方根    |
| `Math.PI`    | 圆周率        |

### 静态方法

| 方法            | 作用                           |
| --------------- | ------------------------------ |
| `Math.abs()`    | 返回一个数的绝对值             |
| `Math.acos()`   | 返回一个数的反余弦值           |
| `Math.acosh()`  | 返回一个数的反双曲余弦值       |
| `Math.asin()`   | 返回一个数的反正弦值           |
| `Math.cbrt()`   | 返回一个数的立方根             |
| `Math.ceil()`   | 向上取整                       |
| `Math.cos()`    | 返回一个数的余弦值             |
| `Math.floor()`  | 向下取整                       |
| `Math.random()` | 返回一个 0 到 1 之间的伪随机数 |
| `Math.round()`  | 四舍五入                       |
| `Math.sqrt()`   | 返回一个数的平方根             |
| `Math.min()`    | 返回一组数的最小值             |
| `Math.max()`    | 返回一组数的最大值             |

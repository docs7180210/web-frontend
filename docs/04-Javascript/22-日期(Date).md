### 时间戳

一种时间表达方式，表示从 1970-1-1 00:00:00 至今的毫秒数

### 日期

`Date`用来获取本地日期对象

```javascript
// 默认以当前时间初始化日期对像
var date = new Date()
var month = date.getMonth()
console.log(month) // 输出月份索引

// 也可以按照给定的时间戳初始化日期对象
var date = new Date(1601765464567)
const year = date.getFullYear()
console.log(year) // 输出 2020
```

### 实例方法

| 方法                | 作用                                     |
| ------------------- | ---------------------------------------- |
| `getDate()`         | 返回指定日期在一个月中的哪一日(1-31)     |
| `getDay()`          | 返回指定日期在一周中的索引，0 表示星期天 |
| `getFullYear()`     | 返回日期所处的年份                       |
| `getHours()`        | 返回小时值                               |
| `getMilliseconds()` | 返回毫秒数                               |
| `getMinutes()`      | 返回分钟数                               |
| `getMonth()`        | 返回月份索引                             |
| `getSeconds()`      | 返回秒数                                 |
| `getTime()`         | 返回时间戳                               |
| `setDate()`         | 设置日期在一个月中的哪一日               |
| `setFullYear()`     | 设置日期所处的年份                       |
| `setHours()`        | 设置小时值                               |
| `setMilliseconds()` | 设置毫秒数                               |
| `setMinutes()`      | 设置分钟数                               |
| `setMonth()`        | 设置月份索引                             |
| `setSeconds()`      | 设置秒数                                 |
| `setTime()`         | 设置时间戳                               |

### 静态方法

| 方法           | 作用                                       |
| -------------- | ------------------------------------------ |
| `Date.now()`   | 返回当前时间戳                             |
| `Date.parse()` | 解析一个表示事件的字符串，返回对应的时间戳 |

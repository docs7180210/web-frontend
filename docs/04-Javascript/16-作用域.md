### 局部作用域

```javascript
// 函数内部定义的变量只能在函数内部访问
function myFunction() {
  var someVar = 'abc'
  // 函数内部可以访问 someVar
  console.log(someVar)
}

// 这里不能访问
console.log(someVar)
```

### 函数参数作用域

```javascript
// 函数参数属于局部作用域，只能在函数内部访问
function sum(x, y) {
  // 这里可以访问x y
}
```

### 全局作用域

全局变量会挂载到 `window`对象上，可以通过 `varName` 或 `window.varName`访问

```javascript
// 在函数外部定义的变量可以在全局访问，称为全局变量
var somVar = 'abc'
console.log(someVar) // 输出 abc

function myFunction() {
  console.log(someVar) // 输出 abc
}

// 在函数内部声明一个变量时，如果没有使用 var 关键字，也会被挂载到 window 对象上称为一个全局变量
function myFunction2() {
  someVar2 = 'def'
  console.log(someVar2) // 输出 def
}
console.log(someVar2) // 输出 def
```

### 闭包

- 当一个函数 A 内嵌了另一个函数 B，并且 B 访问了 A 中定义的变量，函数 B 就形成了一个闭包

- 通常当一个函数执行结束后，其内部定义的变量会被销毁

- 但是如果内部函数 B 作为外部函数 A 的返回值被外部某个持续存在的变量引用，即使 A 执行结束，A 中定义的局部变量也不会被销毁

```javascript
function funA() {
  var x = 1

  function funB(y) {
    return x + y
  }

  return funB
}

var fun = funA()
```

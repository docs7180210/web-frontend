### 内部脚本

内部脚本是指在 html 中插入一个 `script`标签，脚本代码位于 `script`标签内。

```html
<body>
  <script>
    window.alert('Hello world!')
  </script>
</body>
```

### 外部脚本

通过 `<script src="path/to/script.js" ></script>` 可以应用一个独立的 `.js`文件

```html
<head>
  <script src="path/to/script.js"></script>
</head>
```

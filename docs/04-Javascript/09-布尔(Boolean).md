### 创建布尔值

布尔值表示一个值是真（true）或是假(false)

```javascript
// 通过字面量创建
var bool = true
var bool = false

// 通过 Boolean 构造函数创建
var bool = Boolean(false)

// 使用 Boolean 构造函数时，如果初始值是以下值，则返回 false，否则返回 true
Boolean(0)
Boolean(-0)
Boolean(null)
Boolean('')
Boolean(undefined)
Boolean(false)
Boolean(NaN)
```

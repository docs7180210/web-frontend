### 创建对象

`Object`是拥有属性和方法的数据

```javascript
// 对象属性可以是任何类型的数据
var john = {
  name: 'John',
  age: 28,
  isAdult: true,
  children: ['Mary', 'Jack'],
  sayHello: () ={
    console.log("Hello, I'm John.")
  },
}

// 使用
```

### 操作对象

```javascript
var john = {
  name: 'John',
  age: 28,
  isAdult: true,
  children: ['Mary', 'Jack'],
  sayHello: () ={
    console.log("Hello, I'm John.")
  },
}

// 访问对象属性
console.log(john.name) // 输出 John
console.log(john.children) // 输出 ['Mary', 'Jack']

// 调用对象方法
john.sayHello() // 输出 Hello, I'm John.

// 给对象属性重新赋值
john.age = 25

// 可以给对象添加属性或方法
john.height = '180cm'

// 还可以通过方括号操作对象属性
console.log(john['name'])
john['age'] = 26
john['sayHello']()

// 通过 delete 操作符可以删掉对象的属性
delete john.sayHello
delete john.children
```

### 实例方法

| 方法               | 作用                             |
| ------------------ | -------------------------------- |
| `hasOwnProperty()` | 检测一个对象是否包含某个自有属性 |

### 静态方法

| 方法                | 作用                                                     |
| ------------------- | -------------------------------------------------------- |
| `Object.assign()`   | 将一个或者多个源对象中所有可枚举的自有属性复制到目标对象 |
| `Object.create()`   | 以一个现有对象作为原型，创建一个新对象                   |
| `Object.entries()`  | 返回包含给定对象自有的可枚举字符串键属性的键值对         |
| `Object.freeze()`   | 冻结一个对象，冻结后对象变为只读模式                     |
| `Object.isFrozen()` | 判断一个对象是否被冻结                                   |
| `Object.keys()`     | 返回一个由给定对象自身的可枚举的字符串键属性名组成的数组 |
| `Object.values()`   | 返回一个给定对象的自有可枚举字符串键属性值组成的数组     |

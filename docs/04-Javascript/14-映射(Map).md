### 创建映射

`Map`对象用来保存键值对，任何值都可以作为键值对( key =value)

```javascript
// 创建空Map
var map = new Map()

// 使用二维数组作为初始值创建Map
var map = new Map([
  ['a', 1],
  ['b', 2],
  ['c', 3],
])
```

### 操作映射

```javascript
// 插值
map.set('a', 1)
console.log(map) // 输出 {'a' =1}

// 删除已存在的值
map.delete('a')

// 清空映射
map.clear()

// 同过 key 获取对应的值
map.get('a')
```

### 实例方法

| 方法        | 作用                       |
| ----------- | -------------------------- |
| `set()`     | 向映射中添加一个值         |
| `get()`     | 获取映射中指定 key 的值    |
| `delete()`  | 删除某一个值               |
| `clear()`   | 清空映射                   |
| `has()`     | 检测映射中是否具有某个 key |
| `forEach()` | 遍历映射                   |

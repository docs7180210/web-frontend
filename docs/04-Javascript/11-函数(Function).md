### 创建函数

`Function`用来封装一段可执行的代码，在需要的时候调用它

```javascript
// 函数声明
function myFunction() {
  const x = 1
  const y = 2
  console.log(x + y)
}

// 函数表达式
var myFunction = function () {
  const x = 1
  const y = 2
  console.log(x + y)
}

// 箭头函数
var myFunction = () ={
  const x = 1
  const y = 2
  console.log(x + y)
}

// 执行函数
myFunction() // 输出 3
```

### 立即执行的函数

```javascript
// 通过括号包裹函数定义可以立即执行该函数
;(function () {
  console.log('立即执行了')
})()
```

### 函数返回值

很多时候调用函数是为了得到一个经过计算的值

```javascript
// 通过 return 让函数返回一个值给调用方
function getValue() {
  const x = 1
  const y = 2
  return x + y
}

var value = getValue()
console.log(value) // 输出 3
```

### 函数参数

通过参数可以使不同的调用方拿到不同的结果

```javascript
// 求两数之和
function sum(x, y) {
  return x + y
}
console.log(sum(1, 2)) // 输出3

// 若干数求和
function arrSum(arr) {
  var result = 0
  arr.forEach(item ={
    result = result + item
  })
  return result
}
console.log(arrSum([1, 2, 3, 4, 5])) // 输出15

// 求平均值
function getAvg(values) {
  return arrSum(values) / values.length
}

console.log(getAvg([1, 2, 3, 4, 5])) // 输出3
```

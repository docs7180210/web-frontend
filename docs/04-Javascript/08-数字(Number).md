### 创建数字

```javascript
// 字面量创建
var age = 18

// 通过 Number 构造函数创建
var age = Number(18)
var age = Number('18')

// 带小数点的数字
var score = 99.5
```

### 特殊数字

```javascript
// 以 0 开始的数值会被解释为八进制
var num = 012
console.log(num) // 输出10

// 以 0x 开始的数值会被解释为十六进制
var num = 0x1f
console.log(num) // 输出31

// 无穷大
var num = Infinity

// 非数字
var nanNum = Number('abc')
var nanNum2 = 10 - 'a'
var result = isNaN(num) // isNaN方法用于检测是否为非数字
```

### 实例方法

| 方法              | 作用                                        |
| ----------------- | ------------------------------------------- |
| `toFixed()`       | 返回指定小数位数的表示形式                  |
| `toExponential()` | 返回一个数字的指数形式的字符串，如：1.23e+2 |

### 静态属性

| 属性                       | 作用                                                  |
| -------------------------- | ----------------------------------------------------- |
| `Number.MAX_VALUE`         | 最大值                                                |
| `Number.MIN_VALUE`         | 最小值                                                |
| `Number.NaN`               | 非数字                                                |
| `Number.NEGATIVE_INFINITY` | 负无穷，在溢出时返回                                  |
| `Number.POSITIVE_INFINITY` | 正无穷，在溢出时返回                                  |
| `Number.EPSILON`           | 表示 1 和比最接近 1 且大于 1 的最小 Number 之间的差别 |
| `Number.MIN_SAFE_INTEGER`  | 最小安全整数                                          |
| `Number.MAX_SAFE_INTEGER`  | 最大安全整数                                          |

### 静态方法

| 方法                     | 作用                         |
| ------------------------ | ---------------------------- |
| `Number.parseFloat()`    | 将字符串转换成浮点数         |
| `Number.parseInt()`      | 将字符串转换成整型数字       |
| `Number.isFinite()`      | 判断传递的参数是否为有限数字 |
| `Number.isInteger()`     | 判断传递的参数是否为整数     |
| `Number.isNaN()`         | 判断传递的参数是否为 NaN     |
| `Number.isSafeInteger()` | 判断传递的参数是否为安全整数 |

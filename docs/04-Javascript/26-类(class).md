### 类

类是构造函数的语法糖，用更加简洁和现代的语法实现构造函数的功能

```javascript
class Person {
  eyeColor = 'cat'

  sayHello(name) {
    console.log(`Hello ${name}!`)
  }
}

// 使用 new 操作符可以得到一个类的实例对象
var person = new Person()
person.sayHello('world') // 输出 Hello world!
```

### 构造器

类有一个可选的构造器方法，在类被实例化的时候会自动调用，并接收实例化时传入的参数

```javascript
class Animal {
  name

  constructor(option) {
    this.name = option.name
  }

  sayHello() {
    console.log(`Hello, I'm a ${this.name}.`)
  }
}

var cat = new Animal({ name: 'cat' })
var dog = new Animal({ name: 'dog' })
cat.sayHello()
dog.sayHello()
```

### 静态属性/方法

使用 `static`修饰符可以定义类的静态属性/方法，他们只能被类本身调用，不能被类的实例调用

```javascript
class Animal {
  color

  static sayHello() {
    console.log('Hello!')
  }

  constructor(color) {
    this.color = color
  }
}

const cat = new Animal('white')
console.log(cat.color) // 输出 white
Animal.sayHello() // 输出 Hello!
```

### 继承

一个类可以通过 `extends`继承另一个类定义的属性和方法

```javascript
// 父类
class Father {
  fn1() {
    console.log('1')
  }

  fn2() {
    return '2'
  }
}

// 子类
class Child extends Father {
  fn2() {
    var v2 = super.fn2() // 通过 super 可以调用父类的方法
    console.log(`${v2}333`)
  }

  fn3() {
    console.log('3')
  }
}

var child = new Child()
child.fn1() // 子类继承了父类的 fn1 方法，输出 1
child.fn2() // 子类重写了父类的 fn2 方法，输出 2333
child.fn3() // 输出 3
```

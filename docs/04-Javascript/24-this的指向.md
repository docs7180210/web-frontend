### 全局 this

在全局作用域下声明的方法都挂载在`window`对象下，方法内部的`this`也指向全局对象`window`

```javascript
function myFunction() {
  console.log(this)
}

myFunction() // 输出 window 对象
window.myFunction === myFunction // true
```

### 对象方法中的 this

方法中的`this`指向调用该方法的对象

```javascript
var obj1 = {
  name: 'John',

  sayHello() {
    console.log(`I'm ${this.name}`)
  },
}

obj.sayHello() // sayHello方法被 obj1 调用, 其内部的 this 指向 obj1

var obj2 = { name: 'Jack' }
obj2.sayHello = obj1.sayHello
obj2.sayHello()
```

### 修改 this

通过 `apply`、`call`可以在调用方法时指定方法内部 `this`的指向

```javascript
var person1 = {
  name: 'person-1',
}

var person2 = {
  name: 'person-2',
  sayHello: function () {
    console.log(this.name)
  },
}

person2.sayHello() // 输出 person-2
person2.sayHello.call(person1) // 输出 person-1
person2.sayHello.apply(person1) // 输出 person-1

// call 和 apply 方法的区别在于调用方法时传参方式不同
var obj = {
  myFunction(arg1, arg2) {
    // ...
  },
}
obj.myFunction.call(someObj, arg1, arg2)
obj.myFunction.apply(someOjb, [arg1, arg2])
```

### 箭头函数中的 this

箭头函数中的`this`始终指向其定义时的上下文

```javascript
function Person1() {
  this.name = 'person-1'
}

function Person2() {
  this.name = 'person-2'

  this.sayHello = () ={
    console.log(this.name)
  }
}

var person1 = new Person1()
var person2 = new Person2()
person2.sayHello.call(person1) // 输出 person-2
```

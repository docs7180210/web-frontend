### 创建集合

`Set`对象是值的合集，允许存储任何类型的唯一值

```javascript
// 创建空集合
var set = new Set()

// 使用一个数组作为初始值创建集合
var set = new Set([1, 2, 3, 'a', 'b', 'c', { a: 123 }])
```

### 操作集合

```javascript
// 插值
set.add(1)
set.add({ a: 1 })
console.log(set) // 输出 { 1, { a: 1 }}

// 无法向集合中添加一个已经存在的值
set.add(1)
console.log(set) // 输出 { 1, { a: 1 }}

// 删除集合中某一个元素
set.delete(1)

// 清空集合
set.clear()
```

### 实例方法

| 方法        | 作用                       |
| ----------- | -------------------------- |
| `add()`     | 添加新元素                 |
| `clear()`   | 清空集合                   |
| `delete()`  | 删除指定元素               |
| `forEach()` | 遍历集合                   |
| `has()`     | 检测集合中是否存在某一个值 |

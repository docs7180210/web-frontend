### 创建字符串

```javascript
// 通过字面量创建字符串
var str1 = '猫咪'
var str2 = '大黄'

// 通过 String 构造函数创建字符串
var str3 = String('abc')
var str4 = String(111)

// 引号转义
var str5 = "I'm from China."
```

### 操作字符串

```javascript
// 可以使用索引位置来访问字符串中任意一个字符，索引值从 0 开始
var str1 = 'abcde';
var letter = str1[0];
console.log(letter); // 输出 a

// 通过 length 属性可以计算出字符串的长度
var str2 = 'abc';
var length = str2.length;
console.log(length); // 输出 3

// 通过加号(+)链接字符串
var str3 = 'abc';
var str4 = 'def';
var str5 = str3 + str4 + 'ghi';
console.log(str5); // 输出 abcdefghi

// 模板字符串可以不借助加号将字符串和变量链接在一起
var str6 = 'Li lei';
var str7 = `My name is ${str6}`;
console.log(str7); // 输出 My name is Li lei

// 使用字符串的 slice(m, n) 方法提取索引在 m 到 n 之间的片段
var str8 = 'Hello world!';
var str9 str8.slice(6, 8); // 输出 wo
```

### 字符串常用实例方法

| 方法            | 作用                                                                  |
| --------------- | --------------------------------------------------------------------- |
| `charAt()`      | 返回在指定位置的字符                                                  |
| `concat()`      | 连接两个或更多字符串，并返回新的字符串                                |
| `endsWith()`    | 判断当前字符串是否是以指定的子字符串结尾的（区分大小写                |
| `indexOf()`     | 返回某个指定的字符串值在字符串中首次出现的位置                        |
| `includes()`    | 查找字符串中是否包含指定的子字符串                                    |
| `lastIndexOf()` | 从后向前搜索字符串，并从起始位置（0）开始计算返回字符串最后出现的位置 |
| `replace()`     | 在字符串中查找匹配的子串，并替换与正则表达式匹配的子串                |
| `slice()`       | 提取字符串的片断，并在新的字符串中返回被提取的部分                    |
| `split()`       | 把字符串分割为字符串数组                                              |
| `startsWith()`  | 查看字符串是否以指定的子字符串开头                                    |
| `substr()`      | 从起始索引号提取字符串中指定数目的字符                                |
| `substring()`   | 提取字符串中两个指定的索引号之间的字符                                |
| `toLowerCase()` | 把字符串转换为小写                                                    |
| `toUpperCase()` | 把字符串转换为大写                                                    |
| `trim()`        | 去除字符串两边的空白                                                  |

[更多字符串方法](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String)

### 特殊字符串

| 代码 | 输出        |
| ---- | ----------- |
| `\'` | 单引号      |
| `\"` | 双引号      |
| `\\` | 反斜杠      |
| `\n` | 换行        |
| `\r` | 回车        |
| `\t` | tab(制表符) |
| `\b` | 退格符      |
| `\f` | 换页符      |

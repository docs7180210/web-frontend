### Navigator

`navigator`对象包含有关浏览器的信息。

### 对象属性

| 属性            | 说明                                         |
| --------------- | -------------------------------------------- |
| `appCodeName`   | 返回浏览器的代码名                           |
| `appName`       | 返回浏览器的名称                             |
| `appVersion`    | 返回浏览器的平台和版本信息                   |
| `cookieEnabled` | 返回指明浏览器中是否启用 `cookie` 的布尔值   |
| `platform`      | 返回运行浏览器的操作系统平台                 |
| `userAgent`     | 返回由客户机发送服务器的 user-agent 头部的值 |
| `geolocation`   | 返回浏览器的地理位置信息                     |
| `language`      | 返回浏览器使用的语言                         |
| `onLine`        | 返回浏览器是否在线                           |
| `product`       | 返回浏览器使用的引擎                         |

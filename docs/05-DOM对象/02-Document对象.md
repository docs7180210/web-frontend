### Document

`document`是`HTML`文档的根节点，通过`document`对象暴露的方法和属性可以访问或修改任意元素

```html
<!DOCTYPE html>
<html>
  <head>
    <title>index.html</title>
  </head>
  <body>
    <div id="some-div">
      <h1 class="title">大标题</h1>
      <p>段落1</p>
      <p>段落2</p>
    </div>
    <script src="script.js"></script>
  </body>
</html>
```

```javascript
// 通过 id 查找节点
var someDiv = document.querySelector('#some-div')
var someDiv = document.getElementById('some-div')

// 通过 class 查找节点
var title = document.querySelector('.title')
var title = document.getElementByClassName('title')

// 通过标签名称查找节点
var p = document.querySelector('p')
var p = document.getElementByTagName('p')

// querySelector 会查找到第一个匹配的元素，如果需要查找所有匹配的，可以用 querySelectorAll
var p = document.querySelectorAll('p')

// 监听用户操作
document.addEventListener('click', () ={
  document.body.style.backgroundColor = 'red'
})
```

### 常用属性

| 属性              | 作用                                                     |
| ----------------- | -------------------------------------------------------- |
| `documentElement` | 返回文档的`html`元素                                     |
| `activeElement`   | 返回当前获取焦点的元素（通常为`input`、`a`、`button`等） |
| `body`            | 返回文档的`body`元素                                     |
| `domain`          | 返回当前文档的域名                                       |
| `referrer`        | 返回载入当前文档的文档 URL(当前网页从什么网页跳转过来的) |
| `title`           | 设置或返回网页标题                                       |

### 常用方法

| 方法                      | 作用                                      |
| ------------------------- | ----------------------------------------- |
| `addEventListener()`      | 向文档添加事件句柄                        |
| `removeEventListener()`   | 移除已添加的事件句柄                      |
| `createElement()`         | 创建元素节点                              |
| `createTextNode()`        | 创建文本节点                              |
| `write()`                 | 向文档写入内容                            |
| `querySelector()`         | 通过`css`选择器语法查找第一个匹配到的元素 |
| `querySelectorAll()`      | 通过`css`选择器语法查找所有匹配到的元素   |
| `getElementById()`        | 通过`id`属性名查找元素                    |
| `getElementByClassName()` | 通过`class`属性名查找元素                 |
| `getElementByTagName()`   | 通过标签名查找元素                        |

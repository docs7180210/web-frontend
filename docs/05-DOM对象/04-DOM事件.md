### DOM 事件

通过监听 DOM 元素的事件，并执行回调方法，可以为网页添加丰富的交互功能

```html
<!-- 为dom元素绑定点击事件 -->
<button onclick="clickMe()">点我</button>
<script>
  function clickMe() {
    alert('你点我了')
  }
</script>

<!-- 通过 addEventListener 监听dom元素的指定事件 -->
<input id="input" value="" />

<script>
  document.querySelector('#input').addEventListener('input', event ={
    console.log(event.target.value) // 输出输入框的值
  })
</script>
```

### 鼠标事件

| 事件            | 触发时机                 |
| --------------- | ------------------------ |
| `onclick`       | 元素被点击               |
| `oncontextmemu` | 元素被右键点击           |
| `ondbclick`     | 元素被双击               |
| `onmousedown`   | 鼠标按钮按下             |
| `onmouseup`     | 鼠标按钮松开             |
| `onmouseenter`  | 鼠标指针进入元素         |
| `onmouseleave`  | 鼠标指针离开元素         |
| `onmousemove`   | 鼠标移动                 |
| `onmouseover`   | 鼠标指针进入元素或子元素 |
| `onmouseout`    | 鼠标指针离开元素或子元素 |
|                 |                          |

### 键盘事件

| 事件         | 描述               |
| ------------ | ------------------ |
| `onkeydown`  | 键盘按键按下       |
| `onkeypress` | 键盘按键按下再松开 |
| `onkeyup`    | 键盘按键松开       |

### 表单事件

| 事件       | 描述             |
| ---------- | ---------------- |
| `onblur`   | 元素失去焦点     |
| `onchange` | 表单元素内容改变 |
| `onfocus`  | 元素获取焦点     |
| `oninput`  | 向输入框输入内容 |
| `onreset`  | 重置表单         |
| `onsubmit` | 提交表单         |

### 剪贴板事件

| 事件      | 描述       |
| --------- | ---------- |
| `oncopy`  | 拷贝内容时 |
| `oncut`   | 剪切内容时 |
| `onpaste` | 粘贴内容时 |

### 其他事件

| 事件               | 描述                   |
| ------------------ | ---------------------- |
| `onmessage`        | 收到消息               |
| `onwheel`          | 鼠标滚动滚轮           |
| `ononline`         | 浏览器网络在线         |
| `onoffline`        | 浏览器网络离线         |
| `onpopstate`       | 窗口浏览历史发生改变   |
| `onstorage`        | web storage 更新       |
| `visibilitychange` | 页面可见性状态发生变化 |

### 事件捕获

事件流分为 3 个阶段

- 捕获阶段：从文档根节点开始沿着 DOM 树向下传播，直到事件目标元素

- 目标阶段：事件到达目标元素

- 冒泡阶段：从目标元素开始沿着 DOM 树向上传播，直到文档根节点

```javascript
// 事件默认在冒泡阶段触发
element.addEventListener('click', () ={
  console.log('click')
})

// 监听事件时为第三个参数传入 true，可以让事件在捕获阶段触发
element.addEventListener(
  'click',
  () ={
    console.log('click')
  },
  false,
)
```

### 事件委托

当需要对一组列表里的每个元素添加事件监听时，可以将监听器委托给共同的父元素

```html
<ul>
  <li>1</li>
  <li>2</li>
  <li>3</li>
  <li>4</li>
</ul>

<script>
  document.querySelector('ul').addEventListener('click', (event) ={
    if (event.target.tagName === 'LI') {
      console.log(event.target.)
    }
  })
</script>
```

### 事件对象

当监听的事件被触发时，可以从事件对象获取关于事件的详细信息，比如点击事件被触发时获取鼠标相对于屏幕的坐标，或键盘事件被触发时获取触发事件的按键标识符

```javascript
element.addEventListener('click', event ={
  console.log(event.clientX) // 输出鼠标点击时指针相对于浏览器窗口水平坐标
})

input.addEventListener('keypress', event ={
  console.log(event.key) // 输出按键标识符
})
```

#### 事件对象属性

| 属性            | 说明                                         |
| --------------- | -------------------------------------------- |
| `currentTarget` | 指向监听该事件的元素                         |
| `target`        | 指向触发该事件的元素                         |
| `type`          | 返回事件名称                                 |
| `clientX`       | 鼠标事件中，指针相对浏览器窗口的水平坐标     |
| `clientY`       | 鼠标事件中，指针相对浏览器窗口的垂直坐标     |
| `screenX`       | 鼠标事件中，指针相对设备屏幕的水平坐标       |
| `screenY`       | 鼠标事件中，指针相对设备屏幕的垂直坐标       |
| `key`           | 键盘事件中，触发事件的按键标识符             |
| `keyCode`       | 键盘事件中，触发事件的按键编码               |
| `ctrlKey`       | 鼠标/键盘事件被触发时，`Ctrl`按键是否被按下  |
| `shiftKey`      | 鼠标/键盘事件被触发时，`Shift`按键是否被按下 |

#### 事件对象方法

| 方法                | 描述               |
| ------------------- | ------------------ |
| `preventDefault()`  | 禁止事件的默认行为 |
| `stopPropagation()` | 阻止事件继续派发   |

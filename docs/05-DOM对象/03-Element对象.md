### Element

每一个`Element`都是一个元素节点，元素节点的子节点可以是：元素节点、文本节点

```javascript
// 创建一个元素
var divElement = document.createElement('div')

// 设置尺寸和背景色
divElement.style.width = '100px'
divElement.style.height = '100px'
divElement.style.backgroundColor = 'red'

// 获取body元素
var bodyElement = document.body

// 将创建的元素插入到 body 元素内
bodyElement.appendChild(divElement)
```

### 常用属性

| 属性                     | 作用                                       |
| ------------------------ | ------------------------------------------ |
| `attributes`             | 返回元素的属性对象                         |
| `childNodes`             | 返回元素的所有子节点                       |
| `children`               | 返回元素的子元素                           |
| `className`              | 设置或返回元素的`class`属性                |
| `classList`              | 返回元素的类名对象                         |
| `clientTop`              | 返回元素上边框的宽度                       |
| `clientLeft`             | 返回元素左边框的宽度                       |
| `clientHeight`           | 返回元素可视高度（内容 + 内边距）          |
| `clientWdith`            | 返回元素可视宽度（内容 + 内边距）          |
| `firstElementChild`      | 返回第一个子元素                           |
| `firstChild`             | 返回第一个子节点                           |
| `id`                     | 设置或返回元素的`id`属性                   |
| `innerHTML`              | 设置或返回元素的内容                       |
| `nextSibling`            | 返回元素的下一个兄弟节点                   |
| `nextElementSibling`     | 返回元素的下一个兄弟元素                   |
| `nodeType`               | 返回元素的节点类型                         |
| `offsetHeight`           | 元素高度（含边框）                         |
| `offsetWidth`            | 元素宽度（含边框）                         |
| `offsetParent`           | 返回元素最近的定位祖先元素                 |
| `offsetLeft`             | 返回元素相对于`offsetParent`的左侧偏移距离 |
| `offsetTop`              | 返回元素相对于`offsetParent`的顶部偏移距离 |
| `parentNode`             | 返回元素的父节点                           |
| `previousSibling`        | 返回元素的前一个兄弟节点                   |
| `previousElementSibling` | 返回元素的前一个兄弟元素                   |
| `scrollHeight`           | 元素高度（含滚动条）                       |
| `scrollWdith`            | 元素宽度（含滚动条）                       |
| `scrollTop`              | 元素垂直方向已滚动的距离                   |
| `scrollLeft`             | 元素水平方向已滚动的距离                   |
| `style`                  | 设置或指定元素样式                         |
| `tagName`                | 返回元素的大写标签名                       |
| `title`                  | 返回或设置元素的`title`属性值              |

### 常用方法

| 方法                      | 作用                                                |
| ------------------------- | --------------------------------------------------- |
| `addEventListener()`      | 向元素添加事件句柄                                  |
| `removeEventListener()`   | 移除已添加的事件句柄                                |
| `appendChild()`           | 向元素添加一个子元素                                |
| `removeChild()`           | 移除元素的子元素                                    |
| `focus()`                 | 使元素获取焦点                                      |
| `getAttribute()`          | 获取元素指定属性的属性值                            |
| `querySelector()`         | 在子元素中通过`css`选择器语法查找第一个匹配到的元素 |
| `querySelectorAll()`      | 在子元素中通过`css`选择器语法查找所有匹配到的元素   |
| `getElementById()`        | 在子元素中通过`id`属性名查找元素                    |
| `getElementByClassName()` | 在子元素中通过`class`属性名查找元素                 |
| `getElementByTagName()`   | 在子元素中通过标签名查找元素                        |
| `hasAttribute()`          | 检测元素是否具有某个属性                            |
| `hasAttributes()`         | 检测元素是否具有任意属性                            |
| `hasChildNodes()`         | 检测元素是否具有任意子元素                          |
| `hasFocus()`              | 检测元素是否被聚焦                                  |
| `insertBefore()`          | 在子元素前面插入一个元素                            |
| `removeAttribute()`       | 删除指定属性                                        |
| `removeChild()`           | 删除指定的子元素                                    |
| `replaceChild()`          | 替换指定的子元素                                    |
| `setAttribute()`          | 设置指定属性的属性值                                |

### 媒体查询

同一个页面，在不同尺寸的设备上访问时显示不同的样式

```css
/* 设备宽度小于等于 480px 时设置 body 背景色为绿色 */
@media screen and (max-width: 480px) {
  body {
    background-color: green;
  }
}

/* 设备宽度在 481px 至 1024px 之间时设置 body 背景为红色 */
@media screen and (min-width: 481px) and (max-width: 1024px) {
  body {
    background-color: red;
  }
}
```

### 浮动

让一个元素向左或向右靠
`float: left | right;`

| 值      | 说明   |
| ------- | ------ |
| `left`  | 左浮动 |
| `right` | 右浮动 |

```html
<style>
  .menu-item {
    float: left;
  }
</style>
<div>
  <div class="menu-item">菜单一</div>
  <div class="menu-item">菜单二</div>
  <div class="menu-item">菜单三</div>
  <div class="menu-item">菜单四</div>
  <div>环绕元素</div>
</div>
```

### 清除浮动

浮动元素后面的元素会围绕在它周围，如果期望后面的元素不受影响则需要清除浮动
`clear: left | right | both;`

| 值      | 说明       |
| ------- | ---------- |
| `left`  | 清除左浮动 |
| `right` | 清除右浮动 |
| `both`  | 全部清除   |

```html
<style>
  .menu-item {
    float: left;
  }
  .clear {
    clear: both;
  }
</style>
<div>
  <div class="menu-item">菜单一</div>
  <div class="menu-item">菜单二</div>
  <div class="menu-item">菜单三</div>
  <div class="menu-item">菜单四</div>
  <div class="clear"></div>
  <div>另起一行</div>
</div>
```

### 边框

设置元素边框宽度、风格、颜色

| 边框风格 | 说明   |
| -------- | ------ |
| `solid`  | 实线   |
| `double` | 双实线 |
| `dotted` | 点线   |
| `dashed` | 虚线   |

```css
/* 设置边框样式 */
.div-1 {
  border: 1px solid red;
}

/* 分别设置宽度、风格、颜色属性 */
.div-2 {
  border-width: 1px;
  border-style: dotted;
  border-color: red;
}

/* 设置上边框样式 */
.div-3 {
  border-top: 1px dashed red;
}

/* 分别设置上边框宽度、风格、颜色属性 */
.div-4 {
  border-top-width: 1px;
  border-top-style: double;
  border-top-color: red;
}
```

### 圆角

设置元素圆角样式

```css
/* 一个值时对4个角生效 */
.div-1 {
  border-radius: 10px;
}

/* 第一个值对左上和右下角生效，第二个值对右上和左下角生效 */
.div-2 {
  border-radius: 10px 15px;
}

/* 第一个值对左上生效，第二个值对右上和左下角生效，第三个值对右下角生效 */
.div-3 {
  border-radius: 10px 15px 10px;
}

/* 属性值分别对左上、右上、右下、左下生效 */
.div-4 {
  border-radius: 10px 15px 10px 15px;
}

/* 单独设置某一个角 */
.div-5 {
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  border-bottom-left-radius: 10px;
}
```

### 阴影

语法：`box-shadow: x-offset y-offset blur spread color inset`

| 值         | 说明                   |
| ---------- | ---------------------- |
| `x-offset` | 向右偏移距离，允许负数 |
| `y-offset` | 向下偏移距离，允许负数 |
| `blur`     | 模糊距离               |
| `spread`   | 阴影大小               |
| `color`    | 颜色                   |
| `inset`    | 是否由外向内投影       |

```css
.div {
  box-shadow: 2px 2px 10px 2px rgba(0, 0, 0, 0.4);
}
```

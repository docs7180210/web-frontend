### 内联样式

在标签内部的 `style` 属性上编写 `css` 规则

```html
<head>
  ...
</head>
<body>
  <p style="color: red;">一段文字</p>
</body>
```

### 内部样式

在 `html` 内部将 `css` 规则书写到 `style` 标签内

```html
<head>
  <style>
    p {
      color: red;
    }
  </style>
</head>
<body>
  <p>一段文字</p>
</body>
```

### 外部样式

通过 `link` 标签加载独立于 `html` 文件之外的样式文件 (`.css` 文件)

```html
<head>
  <link rel="stylesheet" href="path/to/style.css" />
</head>
<body>
  ...
</body>
```

为了实现最好的样式管理和网站性能，最佳实践是使用外部样式表。通过使用外部样式表，你可以将样式与 HTML 结构分离，这样可以使你的网站更容易维护，同时允许多页面共享同样的样式，从而减少代码冗余。

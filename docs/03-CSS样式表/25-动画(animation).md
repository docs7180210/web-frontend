### 定义动画

规定动画起点和终点的样式

```css
@keyframes animationName {
  from {
    width: 100px;
    height: 100px;
    background-color: blue;
  }
  to {
    width: 200px;
    height: 200px;
    background-color: red;
  }
}
```

更精确的控制动画执行到各个阶段时的样式

```css
@keyframes animationName {
  0% {
    width: 100px;
    height: 100px;
    background-color: blue;
  }
  20% {
    height: 200px;
    background-color: yellow;
  }
  40% {
    width: 200px;
    background-color: red;
  }
  60% {
    border-radius: 100px;
    background-color: black;
  }
  100% {
    background-color: green;
  }
}
```

### 应用动画

| 值                          | 说明                                                                                                                                                                              |
| --------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `animation-name`            | 动画名称                                                                                                                                                                          |
| `animation-duration`        | 动画时长                                                                                                                                                                          |
| `animation-timing-function` | 动画执行的速度曲线                                                                                                                                                                |
| `animation-fill-mode`       | 规定动画开始执行和执行完成后应用什么样式： none 默认行为，不应用任何样式；forwards 动画完成后保持最后一帧样式；backwards 动画开始前应用初始关键帧样式；both = forward + backwards |
| `animation-delay: 1s`       | 动画开始前等待时间                                                                                                                                                                |
| `animation-iteration-count` | 动画循环次数，`n`表示具体次数，`infinite`表示无限循环                                                                                                                             |
| `animation-direction`       | 动画是否在下一周期逆向执行：`normal`不；`reverse`逆向执行；`alternate-reverse`执行次数为基数时逆向，偶数时正向；                                                                  |
| `animation-play-state`      | 动画执行状态 `paused`暂停；`running`运行                                                                                                                                          |

```css
.div {
  animation-name: animationName;
  animation-duration: 4s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: infinite;
  animation-direction: reverse;
}
```

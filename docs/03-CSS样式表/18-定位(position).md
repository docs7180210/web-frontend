### 定位

以某一个元素作为参照物，设置目标元素相对参照物的位置偏移量，需配合 ` top``right``bottom``left `使用
语法：
`position: relative | static | absolute | fixed;`

| 值         | 说明                                                           |
| ---------- | -------------------------------------------------------------- |
| `static`   | 默认值，无定位效果                                             |
| `relative` | 相对定位，以自身作为参照物，也作为绝对定位的参照物             |
| `absolute` | 绝对定位，以最近一个设置了 `relative` 定位的祖先元素作为参照物 |
| `fixed`    | 固定定位，以游览器容器作为参照物                               |

```css
/* 父元素 */
.outer-div {
  position: relative;
}

/* 子元素 */
.inner-div {
  position: absolute;
  top: 10px;
  right: 10px;
  bottom: 10px;
  left: 10px;
}
```

### 层级

当多个元素位置重合时，可以给各个元素指定 z 轴层级，层级最大的显示在最上面
`z-index: 99;`

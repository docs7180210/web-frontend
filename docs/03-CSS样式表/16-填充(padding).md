### 填充（内边距）

为元素各个方形设计内边距

```css
/* 单值简写 */
.div-1 {
  padding: 10px;
}

/* 双值简写 上下 左右 */
.div-2 {
  padding: 10px 20px;
}

/* 三值简写 上 左右 下 */
.div-3 {
  padding: 10px 20px 10px;
}

/* 四值简写 上 右 下 左 */
.div-4 {
  padding: 10px 20px 30px 40px;
}

/* 单边设置 */
.div-5 {
  padding-top: 10px;
  padding-right: 10px;
}
```

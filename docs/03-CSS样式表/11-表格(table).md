### 美化表格

通过 css 使表格更美观

```css
/* 给表格单元格和外轮廓加上1像素的黑色实线边框 */
table,
th,
td {
  border: 1px solid #000;
}

/* 表格外轮廓和单元格都有边框时会形成双层边框，通过以下规则折叠为单层边框 */
table {
  border-collapse: collapse;
}

/* 设置表头背景为深灰色 */
th {
  background-color: #ddd;
}
```

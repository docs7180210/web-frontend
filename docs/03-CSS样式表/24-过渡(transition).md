### 过渡

过渡是元素从一种样式逐渐改变为另一种样式的效果

| 属性                                                  | 说明                                                                 |
| ----------------------------------------------------- | -------------------------------------------------------------------- |
| `transition-delay`                                    | 过渡延迟时间                                                         |
| `transition-duration`                                 | 过渡持续时间                                                         |
| `transition-timing-function`                          | 过渡效果速度曲线：`linear`,`ease`,`ease-in`,`ease-out`,`ease-in-out` |
| `transition-property`                                 | 对元素的某一个或任意样式变化执行过渡效果: `all`, `{someProperty}`    |
| `transition: property duration timing-function delay` | 以上 4 个属性的简写形式                                              |

```css
.div {
  width: 100px;
  height: 100px;
  background-color: red;
}

.div:hover {
  width: 200px;
  height: 200px;
  transition: all 2s ease-in 0.5s;
}
```

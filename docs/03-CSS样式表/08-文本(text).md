### 水平对齐

左，中，右，两端对齐
`text-align: left | center | right | justify;`

### 垂直对齐

上，中，下对齐
`vertical-align: top | middle | bottom;`

### 文字修饰

文字划线位置：上划线，中划线(删除线)，下划线
`text-decoration-line: overline | line-through | underline;`
划线样式：实线，双实线，波浪线，虚线，点线
`text-decoration-style: solid | double | wavy | dashed | dotted;`
划线颜色
`text-decoration-color: red;`
缩写
`text-decoration: line style color;`

### 缩进

在一段文字的第一行产生缩进效果
`text-indent: 2em;`

### 单词间距

设置单词之间的距离，以空格作为单词边界
`word-spacing: 10px;`

### 字母间距

设置字母之间的距离
`letter-spacing: 10px;`

### 文字转换

文字大小写转换：小写，首字母大写，全部大写
`text-transform: lowercase | capitalize | uppercase`

### 空字符处理

`white-space: normal | pre | nowrap | pre-wrap | line-wrap;`
`normal`忽略空白
`pre`保留空白
`nowrap`忽略换行

### 文字阴影

语法：
`text-shadow: $x偏移 $y偏移 $模糊半径 $色值;`

```css
.text {
  text-shadow: 3px 3px 5px red;
}
```

### 文字溢出

语法：
`text-overflow: ellipsis;`

```css
.p-1 {
  overflow: hidden; /* 隐藏超出内容 */
  white-space: nowra; /* 设置内容不换行 */
  text-overflow: ellipsis; /* 在尾部显示省略号 */
}
```

### 文字换行

语法：
`overflow-wrap: normal | break-word;` `word-break: normal | break-all;`
说明：
上面两个属性都用于定义文本换行规则，区别之处在于 `overflow-wrap: break-word`会尽可能在不影响单词可读性的前提下换行，而 `word-break-break-all`会直接在单词填充到容器末尾时将其断开。

```css
.p-1 {
  overflow-wrap: break-word;
}
.p-2 {
  word-break: break-all;
}
```

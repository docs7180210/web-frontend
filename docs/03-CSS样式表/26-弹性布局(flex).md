### 弹性盒子

通过设置 `display: flex | inline-flex`指定一个元素为弹性盒子；设置为`flex`时该元素独占一行；

| 属性                        | 说明                                                                                                                        |
| --------------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| `flex-direction`            | 指定盒子内元素排列方向：`row`横向；`row-reverse`横向倒序，`column`纵向，`column-reverse`纵向倒序                            |
| `justify-content`           | 设置弹性元素在主轴上的排列方式；`flex-start`左对齐，`flex-end`右对齐，`center`居中对齐，`space-between`两端对齐             |
| `align-items`               | 设置弹性元素在侧轴上的排列方式；`stretch`拉伸元素适应容器，`center`居中，`flex-start`开始方向对齐，`flex-start`结束方向对齐 |
| `flex-wrap`                 | 设置换行规则：`nowrap`不换行，`wrap`换行，`wrap-reverse`换行且行倒序排列                                                    |
| `flex-flow: direction wrap` | 是`flex-direction`和`flex-wrap`的简写形式                                                                                   |

```css
.flex-container-1 {
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
}

/* 通过 flex 设置双向居中 */
.flex-container-2 {
  width: 400px;
  height: 400px;
  display: flex;
  justify-content: center;
  align-items: center;
}
```

### 弹性元素

| 属性                      | 说明                                                                         |
| ------------------------- | ---------------------------------------------------------------------------- |
| `order: n`                | 设置弹性元素的排列位置，默认为 `0`                                           |
| `align-self`              | 与盒子的 `align-items`取值范围一致，为指定的元素单独设置其在侧轴上的对齐方式 |
| `flex-grow: n`            | 设置元素相对于其他元素的扩展量                                               |
| `flex-shrink: n`          | 设置元素相对于其他元素的收缩量                                               |
| `flex-basis: auto / px`   | 设置元素基准值                                                               |
| `flex: grow shrink basis` | `flex-grow`、`flex-shrink`和`flex-basis`的简写                               |

```css
.flex-item {
  flex: 1 1 20%;
}

.flex-item:nth-of-type(2) {
  align-self: flex-start;
}
```

### 外边距

为元素各个方向设置外边距，为行内元素设置的 `margin-top`和 `margin-bottom`值会被忽略

```css
/* 单值简写 */
.div-1 {
  margin: 10px;
}

/* 双值简写 上下 左右 */
.div-2 {
  margin: 10px 20px;
}

/* 三值简写 上 左右 下 */
.div-3 {
  margin: 10px 20px 10px;
}

/* 四值简写 上 右 下 左 */
.div-4 {
  margin: 10px 20px 30px 40px;
}

/* 单边设置 */
.div-5 {
  margin-top: 10px;
  margin-right: 10px;
}

/* 设置左右边距为 0 可以实现元素水平居中 */
.div-6 {
  margin: 0 auto;
}
```

### 边距重叠

当相邻的元素都在与对方相接的方向设置了外边距时实际距离是较大的那个值

```css
/* 假设 .p-1 和.p-2 为上下相邻关系，在下面的例子中它们的上下间隔距离为 20px */
.p-1 {
  margin-bottom: 10px;
}

.p-2 {
  margin-top: 20px;
}
```

当子元素的上方或下方与父元素之间没有其它元素，子元素那个方向设置的边距会溢出到父元素外面。解决方式是为父元素增加 `overflow: hidden`或子元素的 `margin`改为父元素的 `padding`实现相同的布局效果

```css
.outer {
  overflow: hidden;
}

.inner {
  margin-top: 30px;
}
```

转换可以用来改变元素的形状、大小、位置，但不会改变其占据的空间

### 移动位置

| 值                   | 说明                            |
| -------------------- | ------------------------------- |
| `translate(x, y)`    | 同时改变元素 x, y 方向的位置    |
| `translate(x, y, z)` | 同时改变元素 x, y, z 方向的位置 |
| `translateX(n)`      | 改变元素 x 方向的位置           |
| `translateY(n)`      | 改变元素 y 方向的位置           |
| `translateZ(n)`      | 改变元素 z 方向的位置           |

```css
/* 配合 position 可以让一个自身尺寸未知的元素相对父元素双向居中 */
.outer {
  width: 400px;
  height: 400px;
  position: relative;
}

.inner {
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
}
```

### 缩放尺寸

| 值                 | 说明                            |
| ------------------ | ------------------------------- |
| `scale(x, y)`      | 同时缩放元素 x, y 方向的大小    |
| `scale3d(x, y, z)` | 同时缩放元素 x, y, z 方向的大小 |
| `scaleX(n)`        | 缩放元素 x 方向的大小           |
| `scaleY(n)`        | 缩放元素 y 方向的大小           |
| `scaleZ(n)`        | 缩放元素 z 方向的大小           |

```css
/* 放大至原始尺寸的 2 倍 */
.div-1 {
  width: 100px;
  height: 100px;
  transform: scale(2, 2);
}

/* 缩小至原始尺寸的 0.5 倍 */
.div-2 {
  width: 100px;
  height: 100px;
  transform: scale(0.5, 0.5);
}
```

### 倾斜

| 值                       | 说明                   |
| ------------------------ | ---------------------- |
| `skew(x-angle, y-angle)` | 同时沿 x、y 轴方向倾斜 |
| `skewX(angle)`           | 沿 x 轴方向倾斜        |
| `skewY(angle)`           | 沿 y 轴方向倾斜        |

```css
.div {
  transform: skew(10deg, 10deg);
}
```

### 旋转

| 值               | 说明            |
| ---------------- | --------------- |
| `rotateX(angle)` | 沿 x 轴方向旋转 |
| `rotateY(angle)` | 沿 y 轴方向旋转 |
| `rotateZ(angle)` | 沿 z 轴方向旋转 |

```css
.div {
  width: 100px;
  height: 100px;
  background-color: green;
  transition: all 2s;
}
.div:hover {
  transform: rotateY(180deg);
}
```

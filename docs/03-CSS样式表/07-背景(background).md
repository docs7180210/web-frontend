### 背景颜色

语法：
`background-color: color;`

```css
div {
  background-color: #ddd;
}
```

### 背景图片

语法：
`background-image: url(/path/to/img.jpg);` `background-size: width height;` `background-repeat: x-repeat y-repeat;` `background-position: x y;`

```css
div {
  background-image: url(path/to/img.jpg);
  background-size: 100px 60px;
  background-repeat: repeat norepeat;
  background-position: center center;
}
```

### 渐变背景

线性渐变：
`background-image: linear-gradient($方向, $色值, $色值, ...);`
重复线性渐变：
`backgrond-image: repeating-linear-gradient($方向, $色值 $宽度, $色值 $宽度, ...);`
径向渐变：
`background-image: radial-gradient($色值 $宽度, $色值 $宽度, ...);`

```css
/* 线性渐变 */
.div-1 {
  background-image: linear-gradient(to right, red, yellow);
}

/* 重复线性渐变 */
.div-2 {
  background-image: repeating-linear-gradient(to right, red, yellow);
}

/* 径向渐变 */
.div-3 {
  background-image: radial-gradient(yellow 10%, red 15%, black 20%);
}
```

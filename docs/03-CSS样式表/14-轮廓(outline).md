### 轮廓

在元素外围绘制一条不占位置的轮廓，与 border 的设置方式类似

```css
/* 设置轮廓样式 */
.div-1 {
  outline: 2px solid red;
}

/* 分别设置轮廓宽度、风格、颜色 */
.div-2 {
  outline-width: 2px;
  outline-style: dotted;
  outline-color: red;
}
```

### 轮廓偏移

轮廓线默认紧贴元素边界绘制，可以修改使其相对边界的偏移位置

```css
/* 向外偏移10px */
.div-1 {
  outline-offset: 10px;
}

/* 当值为负数时向内偏移 */
.div-2 {
  outline-offset: -10px;
}
```

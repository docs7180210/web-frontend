### id 选择器

通过 `#id-name`选中元素

```html
<head>
  <style>
    #text {
      color: red;
    }
  </style>
</head>
<body>
  <p id="text">文本</p>
</body>
```

### class 选择器

通过 `.class-name` 选中元素

```html
<head>
  <style>
    .text {
      color: red;
    }
  </style>
</head>
<body>
  <p class="text">文本</p>
</body>
```

### 标签选择器

通过 `tag-name` 选中元素

```html
<head>
  <style>
    p {
      color: red;
    }
  </style>
</head>
<body>
  <p>文本</p>
</body>
```

### 属性选择器

通过 `[propKey="propValue"]`选中元素

```html
<head>
  <style>
    [title='p2'] {
      color: red;
    }
  </style>
</head>
<body>
  <p>文本</p>
  <p title="p2">文本2</p>
</body>
```

### 嵌套选择器

通过层级关系约束选择范围

```html
<head>
  <style>
    .div-1 p {
      color: red;
    }
  </style>
</head>
<body>
  <div class="div-1">
    <p>div-1内部段落</p>
  </div>
  <div class="div-2">
    <p>div-2内部段落</p>
  </div>
</body>
```

### 分组选择器

```html
<head>
  <style>
    .title,
    p,
    a {
      color: red;
    }
  </style>
</head>
<body>
  <h3 class="title">一个标题</h3>
  <p>一段文字</p>
  <a>一个链接</a>
</body>
```

### 伪类

```css
/* 鼠标悬停在元素上 */
p:hover {
}
p:hover a {
}

/* 被选中的表单元素 */
input:checked {
}

/* 被禁用的表单元素 */
input:disabled {
}

/* 以获取焦点的元素 */
input:focus {
}

/* 已启用的表单元素 */
input:enabled {
}

/* 段落的第一行 */
p:first-line {
}

/* 第一个文字 */
p:first-letter {
}

/* 兄弟元素中的第一位 */
li:first-of-type {
}

/* 兄弟元素中的最后一位 */
li:last-of-type {
}

/* 兄弟元素中除了第一位之外的元素 */
li:not(:first-of-type) {
}

/* 兄弟元素中的第 n 位 */
li:nth-of-type(n) {
}

/* 激活的链接 */
a:active {
}

/* 已访问的链接 */
a:visited {
}
```

### 伪元素

```css
/* 在元素起始位置处插入一个伪元素并设置样式 */
p::before {
  content: 'a'
  color: red;
}

/* 在元素结尾处插入一个伪元素并设置样式 */
p::after {
  content: 'a'
  color: red;
}
```

### 文字大小

`font-size: 14px;`

### 文字加粗

设置文字加粗程度
`font-weight: 100 | 200 | 300 | 400 | 500 | bold;`

### 文字风格

`font-style: normal | italic;`

| 值       | 说明     |
| -------- | -------- |
| `normal` | 默认风格 |
| `italic` | 斜体字   |

### 字体

使用电脑中已经安装的字体
`font-family: 黑体, 仿宋;`

### 自定义字体

电脑中没有安装的字体，可以通过自定义字体使用

```css
/* 申明一个自定义字体 */
@font-face {
  font-family: custom-font;
  src: url(path/to/font.woff);
}

/* 使用自定义字体 */
p {
  font-family: custom-font;
}
```

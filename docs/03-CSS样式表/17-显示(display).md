### 显示

设置元素的显示方式
`display: none | block | inline | inline-block | flex | inline-flex;`

| 值             | 说明                                 |
| -------------- | ------------------------------------ |
| `none`         | 不渲染                               |
| `block`        | 块级元素，独占一行，可设置宽高       |
| `inline`       | 行级元素，不独占一行，不可设置宽高   |
| `inline-block` | 行内块级元素，不独占一行，可设置宽高 |
| `flex`         | 弹性盒子，独占一行，可设置宽高       |
| `inline-flex`  | 弹性盒子，不独占一行，可设置宽高     |

```css
span {
  display: block;
  height: 100px;
}

.div {
  display: inline-block;
  width: 60px;
  height: 40px;
}
```

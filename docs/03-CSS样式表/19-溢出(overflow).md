### 溢出

当一个元素的尺寸大于其父元素尺寸时，可以对父元素设置 `overflow`规则控制超出部分如何显示
`overflow: hidden | visible | auto | scroll;`

| 值        | 说明                                 |
| --------- | ------------------------------------ |
| `hidden`  | 隐藏超出部分                         |
| `visible` | 超出部分仍然显示，但是不占用额外空间 |
| `auto`    | 内容溢出时显示滚动条                 |
| `scroll`  | 始终显示滚动条                       |

```css
/* 父元素 */
.outer {
  height: 300px;
  overflow: auto;
}

/* 子元素 */
.inner {
  height: 500px;
}
```

## head

`head`元素包含了文档的元数据，元数据是关于文档的数据，并不是文档内容，因此它不会显示在网页中。举个例子：照片的元数据包含照片的格式、大小、拍摄地点等。

```html
<!DOCTYPE html>
<html>
  <head>
    <!-- 规定html文档使用的字符编码格式 -->
    <meta charset="utf-8" />

    <!-- 设置网页视口显示规则，以下设置视口宽度等于设备宽度，初始缩放倍数为1，不允许用户手动对页面进行缩放操作 -->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <!-- 规定网页的描述信息，给搜索引擎抓取信息用 -->
    <meta name="description" content="这是页面描述信息" />

    <!-- 规定网页的关键字信息，给搜索引擎抓取信息用 -->
    <meta name="keywords" content="关键词1, 关键词2, 关键词3" />

    <!-- 网页作者的名字 -->
    <meta name="author" content="作者名字" />

    <!-- 规定网页的标题，显示在网页标签栏上面 -->
    <title>我的网页</title>

    <!-- 引用外部样式资源 -->
    <link ref="stylesheet" href="style.css" />
  </head>
  <body></body>
</html>
```

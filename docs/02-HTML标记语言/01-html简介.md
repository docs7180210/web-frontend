### html 是什么

`html` 中文译作超文本标记语言，是一种用于标记网页内容及结构的标记语言，它运行在浏览器上，由浏览器来解析。`html` 文件的后缀为 `.html` 或 `.htm`

```html
<!DOCTYPE html>
<html>
  <head>
    <mate charset="utf-8" />
    <title>网页标题</title>
  </head>
  <body>
    <p>一段文字</p>
  </body>
</html>
```

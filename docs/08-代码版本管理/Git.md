### Git

Git 是一个开源的版本控制系统，被广泛用于软件开发过程中的代码版本控制，是世界上最流行的版本控制系统之一。

### 安装

##### 下载安装包

[git 安装包](https://registry.npmmirror.com/binary.html?path=git-for-windows/)

##### 安装客户端

[安装步骤](https://zhuanlan.zhihu.com/p/242540359)

##### 生成 ssh key

[生成步骤](https://zhuanlan.zhihu.com/p/338668934)

### 基本操作

##### 初始化仓库

```sh
git init
```

##### 克隆仓库

```sh
git clone <repositoryUrl>
```

##### 添加文件

将本地项目中的改动提交到本地暂存区

```sh
git add <file>
```

##### 提交更改

将本地暂存区中的记录提交到本地 `git` 仓库

```sh
git commit -m '修复了用户登录报错的 bug'
```

##### 推送分支

将本地 `git` 仓库的提交记录推送到远程仓库

```sh
git push
```

##### 拉取分支

拉取最新远程仓库的代码文件并与本地代码文件进行合并

```sh
git pull
```

##### 分支管理

```sh
# 查看本地分支列表
git branch

# 查看远程分支列表
git branch -a

# 切换分支
git checkout <branchName>

# 新建分支并切换
git checkout -b <branchName>

# 将另一个分支合并到当前所在分支
git merge <branchName>
```
